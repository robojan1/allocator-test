
#include <stdio.h>
#include <vector>
#include <chrono>
#include <algorithm>
#include <numeric>
#include <string>
#include <assert.h>
#include <random>
#include <set>
#include <list>
#include <deque>
#include <array>
#include "allocators.h"

class TimingResults
{
public:
    using clock = std::chrono::high_resolution_clock;
    std::vector<clock::duration> times;

    clock::duration mean() const
    {
        auto sum = std::accumulate(times.begin(), times.end(), clock::duration(0));
        return sum / times.size();
    }
    void dump() const
    {
        for (auto t : times)
        {
            printf("%ld ns\n", t.count());
        }
    }
};

template <class T>
class Measurements
{
public:
    std::vector<T> measurements;

    T mean() const
    {
        auto sum = std::accumulate(measurements.begin(), measurements.end(), T(0));
        return sum / measurements.size();
    }
    void dump() const
    {
        for (auto t : measurements)
        {
            printf("%ld ns\n", t.count());
        }
    }
};

class Allocator
{
    using clock = std::chrono::high_resolution_clock;

public:
    Allocator(std::string name, void *(*malloc)(size_t), void (*free)(void *), size_t (*freeSpace)(void), size_t (*largestBlock)(void)) : _name(std::move(name)), _malloc(malloc), _free(free), _freeSpace(freeSpace), _largestBlock(largestBlock)
    {
    }
    ~Allocator() = default;

    TimingResults mallocTimes;
    TimingResults freeTimes;
    std::vector<size_t> heapFreeSizes;
    Measurements<float> fragmentations;
    std::vector<size_t> allocSizes;

    float fragmentation()
    {
        return 1.0f - static_cast<float>(_largestBlock()) / static_cast<float>(_freeSpace());
    }

    void BenchMarkRandomInterleaved(int maxSize, int num)
    {
        std::default_random_engine generator;
        generator.seed(123456789);
        std::uniform_int_distribution<size_t> distribution(1, maxSize);
        mallocTimes.times.clear();
        mallocTimes.times.reserve(num);
        freeTimes.times.clear();
        freeTimes.times.reserve(num);
        heapFreeSizes.clear();
        heapFreeSizes.reserve(num);
        fragmentations.measurements.clear();
        fragmentations.measurements.reserve(num);
        allocSizes.clear();
        allocSizes.reserve(num);
        size_t startSize = _freeSpace();
        for (int i = 0; i < num; i++)
        {
            size_t size = distribution(generator);
            allocSizes.push_back(size);
            auto start = clock::now();
            void *ptr = _malloc(size);
            auto end = clock::now();
            mallocTimes.times.push_back(end - start);

            start = clock::now();
            _free(ptr);
            end = clock::now();
            freeTimes.times.push_back(end - start);
            heapFreeSizes.push_back(_freeSpace());
            fragmentations.measurements.push_back(fragmentation());
        }
        if (_freeSpace() != startSize)
        {
            printf("[%s] Random %d interleaved: Error freespace %lu not equal to the starting size %lu\n",
                   _name.c_str(), maxSize, _freeSpace(), startSize);
        }
        printf("[%s] Random %d interleaved: Alloc: %lu ns, Free: %lu ns, Fragmentation: %f\n",
               _name.c_str(), maxSize, mallocTimes.mean().count(), freeTimes.mean().count(), fragmentations.mean());
    }

    void BenchMarkRandom(int maxSize, int num, unsigned long seed = 123456789)
    {
        std::default_random_engine generator;
        generator.seed(seed);
        std::array<float, 14> intervals{1.0, 16, 32.0, 128.0, 1023, 1024, 1025, 2047, 2048, 2049, 4095, 4096, 4097, maxSize};
        std::array<float, 14> weights{10, 20/16, 40/16, 10/96, 0.2f, 30, 10/1024,10/1024, 30, 10/2048,10/2048,30, 10/(maxSize - 4096), 10/(maxSize - 4096)};
        std::piecewise_linear_distribution<float> distribution(intervals.begin(), intervals.end(), weights.begin());
        std::bernoulli_distribution coinFlip;
        mallocTimes.times.clear();
        mallocTimes.times.reserve(num);
        freeTimes.times.clear();
        freeTimes.times.reserve(num);
        heapFreeSizes.clear();
        heapFreeSizes.reserve(num);
        fragmentations.measurements.clear();
        fragmentations.measurements.reserve(num);
        allocSizes.clear();
        allocSizes.reserve(num);
        size_t startSize = _freeSpace();
        std::deque<void *> ptrs;
        int numAllocs = 0;
        while (numAllocs < num || ptrs.size() > 0)
        {
            bool doMalloc = numAllocs < num && (ptrs.size() == 0 || coinFlip(generator));
            if (doMalloc)
            {
                size_t size = static_cast<size_t>(distribution(generator));
                allocSizes.push_back(size);
                auto start = clock::now();
                void *ptr = _malloc(size);
                auto end = clock::now();
                mallocTimes.times.push_back(end - start);
                if (ptr != nullptr)
                {
                    numAllocs++;
                    ptrs.push_back(ptr);
                }
            }
            else
            {
                std::uniform_int_distribution<size_t> freeMemIdx(0, ptrs.size() - 1);
                int ptrIdx = freeMemIdx(generator);
                void *ptr = ptrs[ptrIdx];
                ptrs.erase(ptrs.begin() + ptrIdx);
                auto start = clock::now();
                _free(ptr);
                auto end = clock::now();
                freeTimes.times.push_back(end - start);
            }
            heapFreeSizes.push_back(_freeSpace());
            fragmentations.measurements.push_back(fragmentation());
        }
        if (_freeSpace() != startSize)
        {
            printf("[%s] Random %d: Error freespace %lu not equal to the starting size %lu\n",
                   _name.c_str(), maxSize, _freeSpace(), startSize);
        }
        printf("[%s] Random %d: Alloc: %lu ns, Free: %lu ns, Fragmentation: %f\n",
               _name.c_str(), maxSize, mallocTimes.mean().count(), freeTimes.mean().count(), fragmentations.mean());
    }

    void BenchMarkFixedInterleaved(size_t size, int num)
    {
        mallocTimes.times.clear();
        mallocTimes.times.reserve(num);
        freeTimes.times.clear();
        freeTimes.times.reserve(num);
        heapFreeSizes.clear();
        heapFreeSizes.reserve(num);
        fragmentations.measurements.clear();
        fragmentations.measurements.reserve(num);
        allocSizes.clear();
        allocSizes.reserve(num);
        size_t startSize = _freeSpace();
        for (int i = 0; i < num; i++)
        {
            allocSizes.push_back(size);
            auto start = clock::now();
            void *ptr = _malloc(size);
            auto end = clock::now();
            mallocTimes.times.push_back(end - start);

            start = clock::now();
            _free(ptr);
            end = clock::now();
            freeTimes.times.push_back(end - start);
            heapFreeSizes.push_back(_freeSpace());
            fragmentations.measurements.push_back(fragmentation());
        }
        if (_freeSpace() != startSize)
        {
            printf("[%s] Fixed %lu interleaved: Error freespace %lu not equal to the starting size %lu\n",
                   _name.c_str(), size, _freeSpace(), startSize);
        }
        printf("[%s] Fixed %lu interleaved: Alloc: %ld ns, Free: %ld ns, Fragmentation: %f\n",
               _name.c_str(), size, mallocTimes.mean().count(), freeTimes.mean().count(), fragmentations.mean());
    }

private:
    std::string _name;
    std::vector<void *> _allocatedPtrs;
    void *(*_malloc)(size_t size);
    void (*_free)(void *ptr);
    size_t (*_freeSpace)(void);
    size_t (*_largestBlock)(void);
};

static std::vector<Allocator> allocators = {
    {"FreeRTOS Heap4", pvPortMallocHeap4, vPortFreeHeap4, xPortGetFreeHeapSizeHeap4, xPortGetLargestFreeBlockHeap4},
    {"Newlib nano", nano_malloc, nano_free, nano_getFreeSize, nano_getLargestChunk},
};

int main(int argc, char **argv)
{
    printf("Hello world\n");
    pvPortMallocHeap4(0);
    nano_free(nano_malloc(configTOTAL_HEAP_SIZE - 8));

    allocators[1].BenchMarkRandom(32768, 100000, 123456789);

    for (auto &allocator : allocators)
    {
        allocator.BenchMarkFixedInterleaved(16, 100000);
        allocator.BenchMarkFixedInterleaved(64, 100000);
        allocator.BenchMarkFixedInterleaved(1024, 100000);
        allocator.BenchMarkFixedInterleaved(4096, 100000);
        allocator.BenchMarkFixedInterleaved(32768, 100000);
        allocator.BenchMarkRandomInterleaved(32768, 100000);
        allocator.BenchMarkRandom(32768, 100000, 123456789);
        allocator.BenchMarkRandom(32768, 100000, 987654321);
        allocator.BenchMarkRandom(32768, 100000, 1243);
        allocator.BenchMarkRandom(32768, 100000, 124562);
        allocator.BenchMarkRandom(32768, 100000, 1);
    }
    return 0;
}