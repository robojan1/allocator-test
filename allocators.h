#pragma once

#include <cstddef>

#define configTOTAL_HEAP_SIZE (256 * 1024)

void *pvPortMallocHeap4(size_t xWantedSize);
void vPortFreeHeap4(void *pv);
size_t xPortGetFreeHeapSizeHeap4(void);
size_t xPortGetLargestFreeBlockHeap4(void);

void *nano_malloc(size_t s);
void nano_free(void *ptr);
size_t nano_getFreeSize(void);
size_t nano_getLargestChunk(void);
