
NAME := allocator_test

SRCS := main.cpp freertos_heap4/heap_4.cpp newlib/nano-mallocr.cpp

CC := gcc
CXX := g++
LD := g++

CFLAGS += -g 
CXXFLAGS += -g 
LDFLAGS += 

OBJDIR := .obj
DEPDIR := .dep

OBJS := $(addprefix $(OBJDIR)/, $(addsuffix .o, $(SRCS)))
DEPS := $(addprefix $(DEPDIR)/, $(addsuffix .d, $(SRCS)))

.PHONY: all clean
all: $(NAME)

clean:
	rm -rf $(OBJDIR) $(DEPDIR) $(NAME)


$(OBJDIR):
	mkdir -p $(OBJDIR)

$(DEPDIR):
	mkdir -p $(DEPDIR)


$(NAME): $(OBJS)
	@echo Linking $@
	$(Q)$(CROSS_COMPILE)$(LD) -o $@ $(LDFLAGS) $(OBJS)

$(OBJDIR)/%.cpp.o: %.cpp | $(OBJDIR) $(DEPDIR)
	@echo Building C++ $<
	$(Q)mkdir -p $(OBJDIR)/$(dir $*) $(DEPDIR)/$(dir $*)
	$(Q)$(CROSS_COMPILE)$(CXX) -o $@ -c $(CXXFLAGS) -MF $(addprefix $(DEPDIR)/, $*.cpp.d) -MMD $<

include $(wildcard $(DEPS))